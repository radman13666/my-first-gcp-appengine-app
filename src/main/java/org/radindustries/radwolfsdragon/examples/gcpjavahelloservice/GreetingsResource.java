/*
 * Copyright (C) 2019 rad
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.radindustries.radwolfsdragon.examples.gcpjavahelloservice;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;

/**
 * REST Web Service
 *
 * @author rad
 */
@Path("greetings")
public class GreetingsResource {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of GreetingsResource
   */
  public GreetingsResource() {
  }

  /**
   * Retrieves representation of an instance of org.radindustries.radwolfsdragon.examples.gcpjavahelloservice.GreetingsResource
   * @return an instance of java.lang.String
   */
  @GET
  @Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
  public String getJson() {
    return "{\"title\":\"Greetings from Rad\","
            + "\"message\":\"YO! WASSAP!?\","
            + "\"feeling-good\":true"
            + "}";
  }

  /**
   * PUT method for updating or creating an instance of GreetingsResource
   * @param content representation for the resource
   */
  @PUT
  @Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
  public void putJson(String content) {
    System.out.println(content);
  }
}
